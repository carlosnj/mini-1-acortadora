import webapp  # Parent Class, included in other file.
import random as r  # Generates pseudo-random choices
import shelve  # It keeps the application state safe.
import string  # We need it for the random resource string.
from urllib.parse import unquote  # Convert petition strings into something readable.

PORT = 1235
form = """
        <!DOCTYPE html>
        <html lang="en">
          <body>
            <p></p>
            <form action="/" method="POST">
              <h2>{result}</h2>
              <br>
              Please, introduce a URL to short: <input name="url" type="text" />
            <input type="submit" value="Submit" />
            </form>
            <br>urls: {urlList}
          </body>
        </html>
        """


# A random string of N characters and numbers.
def randomStr(N):
    return ''.join(r.choices(string.ascii_lowercase + string.digits, k=N))


# We add the prefix 'https://' to every URL that does not have it.
def validUrls(url):
    complete_url = 'https://'
    if url.startswith('https://') or url.startswith('http://'):
        complete_url = url
    else:
        complete_url += url

    return complete_url


class RandomShort(webapp.Webapp):
    # Dict where we store the client and shorted URLs.
    urls = shelve.open('urls')

    # We keep the three import things about client petition: PROTOCOL, RESOURCE and BODY
    def parse(self, received):
        recibido = received.decode()
        protocol = recibido.split(" ")[0]
        client_resource = recibido.split(" ")[1]

        # In the 'POST' petitions, we make sure there is a 'qs' that starts with 'url=...'.
        if protocol == "POST" and 'url' in recibido.split("\n")[-1]:
            POST_body = recibido.split("url=")[1]
            POST_body = unquote(POST_body)
        else:
            POST_body = None

        client_resource = 'http://localhost:' + str(PORT) + client_resource

        return protocol, client_resource, POST_body

    # We generate the response depending on the client petition.
    def process(self, analyzed):
        protocol, client_resource, POST_body = analyzed
        homepage = 'http://localhost:' + str(PORT) + '/'
        print("analyzed: ", analyzed)
        print("urls: ", list(self.urls.items()))
        # URL shortener page.
        if client_resource == homepage:

            # The client petition was a 'POST' and it does not exist in our dictionary (urls).
            # We control in case that is empty.
            if protocol == 'POST' and not (POST_body in self.urls.values()):
                if POST_body == '':
                    http = '404 Not Found'
                    html = form
                    html = html.format(urlList=list(self.urls.items()), result='HTTP 404 Not Found: You did not '
                                                                               'introduced anything.')
                else:
                    POST_body = validUrls(POST_body)
                    self.urls[homepage + randomStr(6)] = POST_body
                    http = "200 OK"
                    html = form
                    html = html.format(urlList=list(self.urls.items()), result='HTTP 200 OK: The shorter URL was '
                                                                               'successfully created.')

            # The client petition already exists.
            elif POST_body in self.urls.values():
                http = '200 OK'
                html = form
                html = html.format(urlList=list(self.urls.items()), result='HTTP 200 OK: That URL already exists.')

            # Welcome page html message.
            else:
                http = '200 OK'
                html = form
                html = html.format(urlList=list(self.urls.items()), result='Welcome to the URL shortener. Introduce '
                                                                           'a URL to proceed...')

        # Redirection page. If the client resource does not exist, we send him an error.
        else:
            if client_resource not in self.urls.keys():
                http = '404 Not Found'
                html = form
                html = html.format(urlList=list(self.urls.items()), result='HTTP 404 Not Found: The webpage "' +
                                                                           str(client_resource) + '" is not valid.')
            else:
                http = "200 OK"
                html = "<html><head><meta http-equiv='refresh' content = '1;url={destinyURL}'</head></html>"
                html = html.format(destinyURL=self.urls[client_resource])

        return http, html


if __name__ == "__main__":
    testWebRedirect = RandomShort('localhost', PORT)
